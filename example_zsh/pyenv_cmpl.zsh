# Load pyenv automatically by appending
# the following to
# ~/.zprofile (for login shells) and ~/.zshrc (for interactive shells) :

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# alias pyenv='CFLAGS="-I/usr/bin/openssl" LDFLAGS="-L/usr/lib" pyenv'

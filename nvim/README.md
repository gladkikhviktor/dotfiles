# Work with snippet:

```
cd ~/.config/coc/ && ln -s ~/.config/nvim/snippets/ ultisnips
```

# Work with vimspector:

```
cd ~/.local/share/nvim/site/pack/packer/start/vimspector/ &&
rm -rf configurations &&
ln -s ~/.config/nvim/vimspector_configs/ configurations
```

# Start with options

## For startup with ndap debuger

`nvim --cmd "let g:use_ndap=1"`

## For startup with current dir in &runtimepath

`nvim --cmd "set rtp+=."`

## For startup both mod:

1. Set alias in .bashrc: `alias devnvim='nvim --cmd "let g:use_ndap=1" --cmd "set rtp+=."'`
2. Run `devnvim`

### Haskell setup example:

https://www.reddit.com/r/vim/comments/k3ar3i/cocvim_haskelllanguageserver_starter_tutorial_2020/

### TODO:

https://github.com/ThePrimeagen/harpoon  -- add harpoon for harpooniong files
https://github.com/tpope/vim-dadbod
https://github.com/kristijanhusak/vim-dadbod-ui -- manage and UI

Install lemonade for work with ssh
Install lang-servers: 
```npm install -g pyright```



For work plugin comment run: PackerCompile

require "nvim-treesitter.configs".setup {
    context_commentstring = {
        enable = true
    },
    highlight = {
        enable = true
    },
    -- A list of parser names, or "all"
    ensure_installed = { "vimdoc", "luadoc", "lua", "vim", "markdown", "c", "cpp", "python", "go", "html", "http", "json", "jsonc", "make", "yaml", "c_sharp", "javascript", "typescript", "kotlin", "elixir" },
    --
    -- require "treesitter-context".setup {
    --     enable = true,   -- Enable this plugin (Can be enabled/disabled later via commands)
    --     throttle = true, -- Throttles plugin updates (may improve performance)
    --     max_lines = 0,   -- How many lines the window should span. Values <= 0 mean no limit.
    --     patterns = {
    --         -- Match patterns for TS nodes. These get wrapped to match at word boundaries.
    --         -- For all filetypes
    --         -- Note that setting an entry here replaces all other patterns for this entry.
    --         -- By setting the 'default' entry below, you can control which nodes you want to
    --         -- appear in the context window.
    --         default = {
    --             "function",
    --             "method",
    --             "for",
    --             "while",
    --             "if",
    --             "switch",
    --             "case"
    --         }
    --         -- Example for a specific filetype.
    --         -- If a pattern is missing, *open a PR* so everyone can benefit.
    --         --   rust = {
    --         --       'impl_item',
    --         --   },
    --     },
    --     exact_patterns = {}
    -- }
    -- https://github.com/tree-sitter/tree-sitter/blob/master/cli/README.md
}

require 'lspconfig'.elixirls.setup {
    cmd = { "elixir-ls" },
}
require 'after.treesitter.gotmpl'
require 'after.treesitter.comment'

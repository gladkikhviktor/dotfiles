local root_files = {
    -- 'WORKSPACE', -- added for Bazel; items below are from default config
    'pyproject.toml',
    -- 'setup.py',
    -- 'setup.cfg',
    -- 'requirements.txt',
    -- 'Pipfile',
    -- 'pyrightconfig.json',
}

local settings = {
    root_files = root_files,
}

return settings

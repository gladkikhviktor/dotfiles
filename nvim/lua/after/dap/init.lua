local dap = require('dap')
local ui = require("dapui")
local nvtree = require("nvim-tree.api")

ui.setup()

dap.listeners.after.event_initialized["dapui_config"] = function()
    nvtree.tree.close()
    ui.open()
end

-- https://www.compart.com/en/unicode/U+23F8
vim.api.nvim_set_hl(0, "red", { fg = "#FF0D0D" })
vim.fn.sign_define('DapBreakpoint', { text = '⏸', texthl = 'red', linehl = '', numhl = '' })


require('dap.ext.vscode').load_launchjs(nil, {})

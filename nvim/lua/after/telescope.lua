-- https://github.com/nvim-telescope/telescope.nvim/wiki/Configuration-Recipes#file-and-text-search-in-hidden-files-and-directories
local telescope = require("telescope")

telescope.setup({
    pickers = {
        find_files = {
            -- `hidden = true` will still show the inside of `.git/` as it's not `.gitignore`d.
            find_command = { "rg", "--files", "--hidden", "--glob", "!.git/*" },
        },
    },
    defaults = {
        layout_strategy = 'bottom_pane',
        layout_config = { height = 0.4, prompt_position = "bottom" },
    },
})


require("telescope").load_extension "file_browser"

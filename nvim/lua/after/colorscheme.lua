require('kanagawa').setup({
    compile = false,  -- включить компиляцию цветовой схемы
    undercurl = true, -- включить подчеркивания
    commentStyle = { italic = true },
    keywordStyle = { italic = true },
    statementStyle = { bold = true },
    transparent = true,    -- не устанавливать цвет фона
    dimInactive = false,   -- затемнять неактивное окно `:h hl-NormalNC`
    terminalColors = true, -- определить vim.g.terminal_color_{0,17}
    colors = {             -- добавить/изменить цвета темы и палитры
        palette = {},
        theme = { wave = {}, lotus = {}, dragon = {}, all = {} },
    },
    overrides = function(colors) -- добавить/изменить подсветку
        return {}
    end,
    theme = "dragon",    -- Загружать тему "wave", когда опция 'background' не установлена
    background = {       -- сопоставить значение опции 'background' с темой
        dark = "dragon", -- попробовать "dragon" !
        light = "lotus"
    },
})
vim.cmd("colorscheme kanagawa")
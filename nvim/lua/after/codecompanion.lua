require('gen').setup({
    model = "qwen2.5-coder:3b", -- Модель по умолчанию для использования.
    quit_map = "q",             -- Клавиша для закрытия окна ответа.
    retry_map = "<c-r>",        -- Клавиша для повторной отправки текущего запроса.
    accept_map = "<c-cr>",      -- Клавиша для замены предыдущего выбора на последний результат.
    host = "localhost",         -- Хост, на котором работает служба Ollama.
    port = "11434",             -- Порт, на котором слушает служба Ollama.
    display_mode = "split",     -- Режим отображения. Может быть "float", "split" или "horizontal-split".
    show_prompt = "full",       -- Показывает отправленный запрос к Ollama. Может быть true (3 строки) или "full".
    show_model = true,          -- Отображает, какая модель используется в начале сессии чата.
    no_auto_close = true,      -- Никогда не закрывает окно автоматически.
    file = false,               -- Записывает полезную нагрузку во временный файл для сокращения команды.
    hidden = false,             -- Скрывает окно генерации (если true, неявно установит `prompt.replace = true`), требует Neovim >= 0.10
    init = function(options)
        -- Инициализация Ollama, запуск сервера в фоновом режиме
        pcall(io.popen, "ollama serve > /dev/null 2>&1 &")
    end,
    command = function(options)
        -- Формирование команды для службы Ollama
        local body = { model = options.model, stream = true }
        return "curl --silent --no-buffer -X POST http://" .. options.host .. ":" .. options.port .. "/api/chat -d $body"
    end,
    result_filetype = "markdown", -- Настройка типа файла для буфера результата
    debug = false                 -- Печатает ошибки и выполняемую команду.
})


require "ollama".setup({
    model = "qwen2.5-coder:3b",
})

local function get_exist_venv()
    local dir_venv = vim.fs.find('.venv')
    if #dir_venv > 0 then
        print("Venv already exist ", dir_venv[1])
        return dir_venv[1]
    end
    return ''
end

local function get_venv_path()
    local exist = get_exist_venv()
    if exist ~= '' then
        return exist
    end
    local venv = vim.fn.findfile('pyproject.toml', vim.fn.getcwd() .. ';')
    print("get_venv_path.venv ", venv)
    if venv ~= '' then
        local path = vim.fn.trim(vim.fn.system('poetry env info -p'))
        if path == '' then
            print("Ошибка: не удалось получить путь к venv")
            return ''
        end
        return path
    end
    return ''
end

local function create_symlink_to_venv(venv)
    local root = vim.fn.trim(vim.fn.getcwd())
    local name = '.venv'
    local exist = vim.fn.findfile(name, vim.fn.getcwd() .. ';')
    if exist == '' then
        local cmd = 'ln -s ' .. venv .. ' ' .. root .. '/.venv'
        print(cmd)
        local result = vim.fn.system(cmd)
        if result ~= '' then
            print("Ошибка при создании символической ссылки: ", result)
        end
    end
end


require('venv-selector').setup {
    auto_refresh = true,
    --     options = {
    --         on_venv_activate_callback = nil,           -- callback function for after a venv activates
    --         enable_default_searches = true,            -- switches all default searches on/off
    --         enable_cached_venvs = true,                -- use cached venvs that are activated automatically when a python file is registered with the LSP.
    --         cached_venv_automatic_activation = true,   -- if set to false, the VenvSelectCached command becomes available to manually activate them.
    --         activate_venv_in_terminal = true,          -- activate the selected python interpreter in terminal windows opened from neovim
    --         set_environment_variables = true,          -- sets VIRTUAL_ENV or CONDA_PREFIX environment variables
    --         notify_user_on_venv_activation = false,    -- notifies user on activation of the virtual env
    --         search_timeout = 5,                        -- if a search takes longer than this many seconds, stop it and alert the user
    --         debug = true,                              -- enables you to run the VenvSelectLog command to view debug logs
    --         require_lsp_activation = true,             -- require activation of an lsp before setting env variables
    --
    --         -- telescope viewer options
    --         on_telescope_result_callback = nil,     -- callback function for modifying telescope results
    --         show_telescope_search_type = true,      -- shows which of the searches found which venv in telescope
    --         telescope_filter_type = "substring",    -- when you type something in telescope, filter by "substring" or "character"
    --         telescope_active_venv_color = "#00FF00" -- The color of the active venv in telescope
    -- }
}
vim.api.nvim_create_autocmd('VimEnter', {
    desc = 'Auto select virtualenv Nvim open',
    pattern = '*',
    callback = function()
        -- https://github.com/linux-cultist/venv-selector.nvim?tab=readme-ov-file#-automate
        -- local venv = vim.fn.findfile('pyproject.toml', vim.fn.getcwd() .. ';')
        -- print("VENV", venv)
        -- if venv ~= '' then
        --     print("retive from cache")
        --     require('venv-selector').retrieve_from_cache()
        -- end
        local selected = false
        local venv = get_venv_path()
        if venv ~= '' then
            -- require 'venv-selector.venv'.set_venv_and_system_paths({ value = venv })
            print("VENV", venv)
            require 'venv-selector.hooks'.set_python_path_for_client("pyryght", venv)
            selected = true
        end
        -- if selected and get_exist_venv() == '' then
        --     create_symlink_to_venv(venv)
        -- end
    end,
    once = true
})

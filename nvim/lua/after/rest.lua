vim.cmd([[
let g:vrc_auto_format_response_patterns = {
    \ 'json': 'yq',
    \ 'xml': 'tidy -xml -i -'
\}
]])
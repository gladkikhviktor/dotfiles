vim.cmd [[packadd packer.nvim]]
return require('packer').startup(function(use)
    -- Packer can manage itself
    use "wbthomason/packer.nvim"
    use 'nvim-tree/nvim-web-devicons'
    --
    use 'neovim/nvim-lspconfig' -- Configurations for Nvim LSP
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-cmdline'
    use 'hrsh7th/nvim-cmp'
    -- -- For vsnip users.
    use 'hrsh7th/cmp-vsnip'
    use 'hrsh7th/vim-vsnip'
    --
    -- https://github.com/folke/trouble.nvim -- diagnostics message butify
    use "nvim-lua/plenary.nvim"
    use "nvim-lua/popup.nvim"
    use "nvim-treesitter/nvim-treesitter"

    use "nvim-telescope/telescope.nvim"
    use { "nvim-telescope/telescope-file-browser.nvim" }

    -- OutlinePlugin
    use({
        "stevearc/aerial.nvim",
        config = function()
            require("aerial").setup()
        end,
    })

    -- Themes
    -- use { "ellisonleao/gruvbox.nvim" }
    -- use { "NTBBloodbath/sweetie.nvim" }
    use { "EdenEast/nightfox.nvim" }
    use "rebelot/kanagawa.nvim"
    use { "sho-87/kanagawa-paper.nvim" }
    use({ 'projekt0n/github-nvim-theme' })
    use { "scottmckendry/cyberdream.nvim" }
    use { "catppuccin/nvim", as = "catppuccin" }
    -- use "JoosepAlviste/nvim-ts-context-commentstring" --расширяем возможности комментирования
    use {
        'numToStr/Comment.nvim',
        config = function()
            require('Comment').setup()
        end
    }
    use "Chiel92/vim-autoformat"

    use "nvim-lualine/lualine.nvim"

    -- --git flow
    -- use "tpope/vim-fugitive"
    -- use "junegunn/gv.vim"

    use "kyazdani42/nvim-tree.lua"
    use "ryanoasis/vim-devicons"

    -- https://github.com/jbyuki/one-small-step-for-vimkind

    use "neoclide/jsonc.vim"
    use "b0o/schemastore.nvim"

    -- Better plugin for Golang!!!
    use "fatih/vim-go"

    -- D2
    use 'terrastruct/d2-vim'
    -- DAP
    use 'mfussenegger/nvim-dap'
    use { "rcarriga/nvim-dap-ui", requires = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio" } }
    use { 'mfussenegger/nvim-dap-python', requires = { "mfussenegger/nvim-dap" } }
    use 'leoluz/nvim-dap-go'
    -- debug lua plugins
    --  TODO: research this
    use { 'jbyuki/one-small-step-for-vimkind' }

    -- K8S
    use { 'towolf/vim-helm' }
    -- TODO install their
    -- https://github.com/mrjosh/helm-ls
    -- https://github.com/Allaman/kustomize.nvim#build-manifests
    --
    --

    --
    -- use { 'diepm/vim-rest-console' }

    -- Markdown

    -- sh -c "$(curl -s https://raw.githubusercontent.com/ibhagwan/fzf-lua/main/scripts/mini.sh)"
    use { "ibhagwan/fzf-lua",
        requires = { "nvim-tree/nvim-web-devicons", "echasnovski/mini.icons" }
    }
    -- Poetry
    use { 'linux-cultist/venv-selector.nvim', branch = "regexp",
        requires = { 'neovim/nvim-lspconfig', 'nvim-telescope/telescope.nvim', 'mfussenegger/nvim-dap-python' } }

    -- PlantUML
    use {
        'https://gitlab.com/itaranto/plantuml.nvim',
        tag = '*',
        config = function() require('plantuml').setup() end
    }


    -- AI
    use { "David-Kunz/gen.nvim" }
    use { "nomnivore/ollama.nvim", requires = { "nvim-lua/plenary.nvim" } }
end
)

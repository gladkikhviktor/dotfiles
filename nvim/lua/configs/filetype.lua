local http_callback = function() vim.bo.filetype = "http" end
local json_callback = function() vim.bo.filetype = "jsonc" end

vim.api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
    pattern = { "*.http", "*.hurl" },
    callback = http_callback
})

vim.api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
    pattern = { "*.json", "*.jsonc", "*.json.j2" },
    callback = json_callback
})

function FormatPythonWithBlack()
    return vim.api.nvim_exec([[
                 :!black %
            ]], true)
end

function FormatD2()
    return vim.api.nvim_exec([[:!d2 fmt % ]], true)
end

function FormatZSH()
    -- TODO bugs need fix
    return vim.api.nvim_exec2(
        [[:!shfmt -w % ]], {})
end

function FormatCode()
    local type = vim.fn.expand("%:e")
    local result = nil
    if type == "d2" then
        result = FormatD2()
        print("D2 format")
    else
        _ = vim.api.nvim_exec2(
            [[lua vim.lsp.buf.format({ async = true })]], {}
        )
        print("LSP Server format")
    end
    _ = vim.api.nvim_exec2(":redraw", {})
    if not result then
        print("Error")
    end
end

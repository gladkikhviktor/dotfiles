-- LEADER KEY
vim.g.mapleader = ","

-- turn off search highlight
vim.api.nvim_set_keymap("n", "<Esc>", ":nohlsearch<CR>", { noremap = true, silent = true })



vim.api.nvim_set_keymap("n", "<C-h>", '^', { noremap = true })
vim.api.nvim_set_keymap("n", "<C-l>", '$', { noremap = true })
vim.api.nvim_set_keymap("n", "<C-j>", '}', { noremap = true })
vim.api.nvim_set_keymap("n", "<C-k>", '{', { noremap = true })

-- " d => "delete"
vim.api.nvim_set_keymap("n", "x", '"_x', { noremap = true })
vim.api.nvim_set_keymap("n", "d", '"_d', { noremap = true })
vim.api.nvim_set_keymap("n", "D", '"_D', { noremap = true })
vim.api.nvim_set_keymap("v", "d", '"_d', { noremap = true })

-- Indents
vim.api.nvim_set_keymap("v", "<", "<gv", {})
vim.api.nvim_set_keymap("v", ">", ">gv", { noremap = true })

-- Select all
vim.api.nvim_set_keymap("n", "<leader>a", "ggVG", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>ф", "ggVG", { noremap = true })

vim.api.nvim_set_keymap("n", "<leader>s", ":w<CR>", { noremap = true })
-- Next tab
vim.api.nvim_set_keymap("n", "<TAB>", ":bnext<CR>", { noremap = true, silent = true })
-- Prev tab
vim.api.nvim_set_keymap("n", "<S-TAB>", ":bprevious<CR>", { noremap = true, silent = true })


-- Close buffer without save
vim.api.nvim_set_keymap("n", "<M-q>", ":bd!<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<M-й>", ":bd!<CR>", { noremap = true })

-- Control-c instead of escape
vim.api.nvim_set_keymap("n", "<C-c>", "<Esc>", { noremap = true })
vim.api.nvim_set_keymap("n", "<C-с>", "<Esc>", { noremap = true })

-- Format code
vim.api.nvim_set_keymap("n", "<M-f>", ":lua FormatCode()<CR>", { noremap = true, silent = true })
-- vim.api.nvim_set_keymap("n", "<M-а>", ":lua FormatCode()<CR>", { noremap = true })

-- " provide hjkl movements in Insert mode via the <Contorl> modifier key
vim.api.nvim_set_keymap("i", "<C-h>", "<C-o>h", { noremap = true })
vim.api.nvim_set_keymap("i", "<C-р>", "<C-o>h", { noremap = true })

vim.api.nvim_set_keymap("i", "<C-l>", "<C-o>l", { noremap = true })
vim.api.nvim_set_keymap("i", "<C-д>", "<C-o>l", { noremap = true })

vim.api.nvim_set_keymap("i", "<C-j>", "<C-o>j", { noremap = true })
vim.api.nvim_set_keymap("i", "<C-о>", "<C-o>j", { noremap = true })

vim.api.nvim_set_keymap("i", "<C-k>", "<C-o>k", { noremap = true })
vim.api.nvim_set_keymap("i", "<C-л>", "<C-o>k", { noremap = true })

-- Return to normal mode in terminal
vim.api.nvim_set_keymap("t", "<esc>", "<C-\\><C-n>", {})
vim.api.nvim_set_keymap("t", "<C-x>", "<C-\\><C-n>", {})


-- File manager
vim.api.nvim_set_keymap("n", "<M-b>", ":NvimTreeToggle<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<M-и>", ":NvimTreeToggle<CR>", { noremap = true })

-- Terminal
vim.api.nvim_set_keymap("n", "<leader>tg", ":terminal<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>еп", ":terminal<CR>", { noremap = true, silent = true })

-- TELESCOPE config
vim.api.nvim_set_keymap("n", "<leader>f", "::FzfLua files resume=true<cr>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>а", ":FzfLua files resume=true<cr>", { noremap = true })

vim.api.nvim_set_keymap("n", "<leader>g", ":FzfLua live_grep resume=true<cr>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>п", ":FzfLua live_grep resume=true<cr>", { noremap = true })

vim.api.nvim_set_keymap("n", "<leader>b", ":FzfLua buffers resume=true<cr>", { noremap = true })

vim.api.nvim_set_keymap("n", "<leader>h", ":FzfLua help_tags resume=true<cr>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>р", ":FzfLua help_tags resume=true<cr>", { noremap = true })


vim.api.nvim_set_keymap("n", "dw", "diw", { noremap = true })
vim.api.nvim_set_keymap("n", "cw", "ciw", { noremap = true })
vim.api.nvim_set_keymap("n", "yw", "yiw", { noremap = true })
vim.api.nvim_set_keymap("n", "vw", "viw", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>o", "o<esc>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>O", "O<esc>", { noremap = true })
vim.api.nvim_set_keymap("n", "<C-a>", "ggVG", { noremap = true })
vim.api.nvim_set_keymap("n", "<C-s>", ":w<CR>", { noremap = true })
vim.api.nvim_set_keymap("i", "<C-s>", "<esc>:w<cr>", { noremap = true })

-- DAP
vim.api.nvim_set_keymap("n", "<F4>", ":lua require 'dapui'.close()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<F5>", ":lua require 'dap'.continue()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<F6>", ":lua require'dap'.step_over()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<F8>", ":lua require'dap'.step_into()", { noremap = true })
vim.api.nvim_set_keymap("n", "<F9>", ":lua require 'dap'.toggle_breakpoint()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<F10>", ":lua require'dap'.repl.open()<CR>", { noremap = true })


-- AERIAL PLUGIN
vim.keymap.set("n", "<leader>n", "<cmd>AerialToggle!<CR>")
-- vim.keymap.set("n", "{", "<cmd>AerialPrev<CR>", { buffer = bufnr })
-- vim.keymap.set("n", "}", "<cmd>AerialNext<CR>", { buffer = bufnr })
-- <leader>vs', '<cmd>VenvSelect<cr>'
vim.keymap.set('n', '<leader>vs', '<cmd>VenvSelect<cr>')
vim.keymap.set('n', '<leader>vsc', '<cmd>VenvSelectCached<cr>')

vim.api.nvim_command(
    'set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz')
-- Indents
vim.api.nvim_set_keymap("v", "Б", "<gv", {})
vim.api.nvim_set_keymap("v", "Ю", ">gv", { noremap = true })

-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<C-d>', ":FzfLua lsp_document_diagnostics resume=true<CR>", { noremap = true, silent = true })

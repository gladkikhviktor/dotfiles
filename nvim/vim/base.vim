"""File created by motive https://www.chrisatmachine.com/neovim
set path+=**    " Searches current directory recursively.
set mouse=nicr
set mouse=a  " enable mouse
set t_Co=256
set encoding=utf-8
set number
set noswapfile
set scrolloff=9
set cursorline
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set fileformat=unix
filetype indent on      " load filetype-specific indent files
set relativenumber
" For right work install xclip (:help clipboard)
set clipboard=unnamedplus " shared clipboard with system
" TextEdit might fail if hidden is not set.
set hidden
" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300
" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

set signcolumn=yes:1
set wildmenu
set wildmode=longest:full,full
set smartcase
set ignorecase
set nojoinspaces
set splitright
set guicursor=i:block

""" yarn global add lua-fmt

" Always show tabs
set showtabline=0

" We don't need to see things like -- INSERT -- anymore
set noshowmode
set termguicolors   " enable true colors support

" Ignore files
set wildignore+=*.pyc
set matchpairs+=<:>
" Install Fonts
" https://github.com/ryanoasis/nerd-fonts#option-3-install-script
set shell=zsh\ -i " support zsh terminal

" au * <buffer>
set keymap=russian-jcukenwin

set iminsert=0

set imsearch=0

highlight lCursor guifg=NONE guibg=Cyan

-- https://github.com/nanotee/nvim-lua-guide/tree/1cb2b9a92d141a8a1f664fdf24fa9a7bd4a2d30f  -- example config items
vim.g.python3_host_prog = "/home/viktor/.pyenv/versions/nvim/bin/python"
local vimrc = "~/.config/nvim/"
vim.cmd("so " .. vimrc .. "vim/base.vim")
require "plugins"
require "after"
require "configs"


# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
# ZSH_THEME="robbyrussell"
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git pyenv)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
if [ ! -d ~/dotfiles/extends_zsh ]; then
	mkdir -p ~/dotfiles/extends_zsh
fi

alias ls='ls --color=auto'
alias rbc=~/vpn_rbc.sh
alias cls=clear
alias work='cd ~/RBC/projects/'
alias confignvim='cd ~/.config/nvim/ && nvim .'
alias devnvim='nvim --cmd "let g:use_ndap=1" --cmd "set rtp+=."'
alias kowl='kubectl --kubeconfig=/home/viktor/kubeconfig/developers-ro.kubeconfig -n kowl port-forward service/kowl 9001:80'

# alias kowl='kubectl --kubeconfig=/home/viktor/kubeconfig/kowl.kubeconfig -n kowl port-forward service/kowl 9001:80'
# alias kowl='kubectl --kubeconfig=/home/viktor/kubeconfig/pro-test.kubeconfig -n test-pro-statistics-celery-flower-6c7f78dbb9-ch6px port-forward service/kowl 9001:80'
alias jq=~/Programs/jq-linux64
alias todos='cd ~/TODOS/'
alias dotfiles='cd ~/dotfiles/'
alias ngrok='~/Programs/ngrok'
alias nv='nvim'
alias tm='tmux'
alias tmn='tmux new -s '
alias tma='tmux attach -t '
alias tml='tmux ls'
alias love='~/Programs/love-11.4-x86_64.AppImage'

export PATH="$PATH:/home/viktor/.dotnet/tools"
export PATH="$PATH:/home/viktor/go/bin"
export PATH="$PATH:/home/viktor/.sdkman/candidates/java/current/bin"
export PATH="$PATH:/home/viktor/Programs/kotlin-language-server/server/build/install/server/bin"
export EDITOR='nvim'
export NODE_OPTIONS="--dns-result-order=ipv4first"

if [ -f ~/dotfiles/extends_zsh/k3d.compl.zsh ]; then
	autoload -U compinit
	compinit
	source ~/dotfiles/extends_zsh/k3d.compl.zsh
fi

if [ -f ~/dotfiles/extends_zsh/skaffold.compl.zsh ]; then
	source ~/dotfiles/extends_zsh/skaffold.compl.zsh
fi

if [ -f ~/dotfiles/extends_zsh/kubectl_compl.zsh ]; then
	source ~/dotfiles/extends_zsh/kubectl_compl.zsh
fi

if [ -f ~/dotfiles/extends_zsh/pyenv_cmpl.zsh ]; then
	source ~/dotfiles/extends_zsh/pyenv_cmpl.zsh
fi

if [ -f ~/dotfiles/extends_zsh/flutter_cmpl.zsh ]; then
	source ~/dotfiles/extends_zsh/flutter_cmpl.zsh
fi

# The next line updates PATH for Yandex Cloud CLI.
if [ -f '/home/viktor/yandex-cloud/path.bash.inc' ]; then source '/home/viktor/yandex-cloud/path.bash.inc'; fi

# The next line enables shell command completion for yc.
if [ -f '/home/viktor/yandex-cloud/completion.zsh.inc' ]; then source '/home/viktor/yandex-cloud/completion.zsh.inc'; fi
#
#
#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

# https://github.com/sindresorhus/guides/blob/main/npm-global-without-sudo.md
NPM_PACKAGES="${HOME}/.npm-packages"

export PATH="$PATH:$NPM_PACKAGES/bin"

# Preserve MANPATH if you already defined it somewhere in your config.
# Otherwise, fall back to `manpath` so we can inherit from `/etc/manpath`.
export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"

#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

export PS1="\[\033[36m\]\u\[\033[m\]\[\033[32m\] \[\033[33;1m\]\w\[\033[m\] \$(git branch 2>/dev/null | grep '^*' | colrm 1 2) \$ "

source /usr/share/bash-completion/completions/git

alias rbc=~/vpn_rbc.sh
alias cls=clear
alias work='cd ~/RBC/projects/'
alias confignvim='cd ~/.config/nvim/ && nvim .'
alias devnvim='nvim --cmd "let g:use_ndap=1" --cmd "set rtp+=."'
alias kowl='kubectl --kubeconfig=/home/viktor/kubeconfig/kowl.kubeconfig -n kowl port-forward service/kowl 9001:80'
alias jq=~/Programs/jq-linux64
alias todos='cd ~/TODOS/'
alias dotfiles='cd ~/dotfiles/'

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

eval "$(pyenv virtualenv-init -)"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
. "$HOME/.cargo/env"

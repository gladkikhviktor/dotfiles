```
cd ~/.config &&
rm -rf nvim &&
ln -s ~/dotfiles/nvim nvim
```

```
cd ~/.config 
rm -rf kitty 
ln -s ~/dotfiles/kitty kitty 
```

```
ln -s ~/dotfiles/.bashrc ~/.bashrc
```

```
ln -s ~/dotfiles/.zshrc ~/.zshrc
```

```
ln -s ~/dotfiles/.zshrc_manjaro ~/.zshrc
```
```
mkdir -p $HOME/.config/alacritty/
ln -s ~/dotfiles/alacrity/alacritty.yml $HOME/.config/alacritty/alacritty.yml
```
```
ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf 
tmux source ~/.tmux.conf
```
```
git clone git@github.com:ryanoasis/nerd-fonts.git 
cd nerd-fonts
./install.sh FiraCode
```
```
git config --global core.excludesFile '/home/viktor/dotfiles/global/.gitignore'
```
```
ln -s ~/dotfiles/.ideavimrc ~/.ideavimrc
```
